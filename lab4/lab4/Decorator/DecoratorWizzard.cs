﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using lab4.Flyweight;

namespace lab4.Decorator
{
    class DecoratorWizzard : Wizzard
    {
        protected Unit DecoratedWizzard { get; set; }

        public DecoratorWizzard(Unit decoratedWizzard)
        {
            DecoratedWizzard = decoratedWizzard;
        }

        public override void Voice()
        {
            DecoratedWizzard.Voice();
        }
    }
}
