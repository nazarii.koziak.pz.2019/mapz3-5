﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using lab4.Flyweight;


namespace lab4.Decorator
{
    class BossOfThisGym : DecoratorWizzard
    {
        public BossOfThisGym(Unit decoratedWizzard)
            : base(decoratedWizzard)
        {
        }

        public override void Voice()
        {
            Console.WriteLine("*Oh no... BOSS IS COMING!*");
            base.Voice();
            Console.WriteLine("I`M A TRUE BOSS OF THIS GYM, AND I WILL BEAT YOU!");
        }
    }
}
