﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace lab4.Facade
{
    class Calendar
    {
        public List<Task> Tasks;
        public List<DateTime> Dates;
        public List<Timer> TIme;

        public void CreateEvent()
        {
            Console.WriteLine("Event created!");
        }

        public void DeleteEvent()
        {
            Console.WriteLine("Event deleted!");
        }

        public void RepeatEvent()
        {
            Console.WriteLine("Event put on repeat!");
        }
    }
}
