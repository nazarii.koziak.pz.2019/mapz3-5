﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab4.Facade
{
    class Statistics
    {
        public int CompletedTasks;
        public int ExpiredTasks;
        public int Days;
        public double GeneralEfficiency;
    }
}
