﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace lab4.Facade
{
    class Task
    {
        public string TaskName;
        public string Description;
        public Timer ExecutionTIme;
        public DateTime ExecutionDate;
        public Status TaskStatus;

        public Task(string taskName, string description, Timer executionTIme, DateTime executionDate, Status taskStatus)
        {
            TaskName = taskName;
            Description = description;
            ExecutionTIme = executionTIme;
            ExecutionDate = executionDate;
            TaskStatus = taskStatus;
        }
    }
}
