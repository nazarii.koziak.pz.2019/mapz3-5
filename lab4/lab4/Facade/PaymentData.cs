﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab4.Facade
{
    class PaymentData
    {
        public ServiceOftransaction serviceOftransaction;
        public int ID;
        public DateTime ExpiredDate;

        public bool Verification()
        {
            if (DateTime.Now > ExpiredDate)
                return false;
            else
            {
                Console.WriteLine("Still worthy");
                return true;
            }

        }
    }
}
