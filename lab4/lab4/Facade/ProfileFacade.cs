﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab4.Facade
{
    class ProfileFacade
    {
        private User m_User;
        private Statistics m_Statistics;
        private PaymentData m_PaymentData;
        private Notification m_Notification;

        public void ChangeInfo( User us)
        {
            //... Тут мала бути дуже складна логіка...
            Console.WriteLine("Info Changed!");
        }
        public void ChangePaymentData(PaymentData pd)
        {
            //... Тут мала бути дуже складна логіка...
            Console.WriteLine("Payment Data is changed!");
        }

        public void ResetStatistic()
        {
            //... Тут мала бути дуже складна логіка...
            Console.WriteLine("Statistic is reseted!");
        }

        public void SaveProgress()
        {
            //... Тут мала бути дуже складна логіка...
            Console.WriteLine("Proges is saved!");
        }

        public void Share()
        {
            //... Тут мала бути дуже складна логіка...
            Console.WriteLine("Your result is Shared!");
        }

        public void CutomizeNotification()
        {
            //... Тут мала бути дуже складна логіка...
            Console.WriteLine("Notification is changed!");
        }
    }
}
