﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab4.Facade
{
    enum Status
    {
        NotStarted,
        InProgress,
        Completed,
        Delayed
    }
}
