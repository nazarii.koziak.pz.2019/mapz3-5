﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab4.Flyweight
{
    class Warrior : Unit
    {
        public Warrior()
        {
            Name = "Barbarian";
            Health = 30;
            Damage = 10;
            picture = UnitFlyweight.CreateWarrior();
        }
    }
}
