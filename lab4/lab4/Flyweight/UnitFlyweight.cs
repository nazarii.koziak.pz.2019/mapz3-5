﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab4.Flyweight
{
    class UnitFlyweight
    {
        public static Dictionary<Type, Image> Images = new Dictionary<Type, Image>();

        public static Image CreateWarrior()
        {
            if(!Images.ContainsKey(typeof(Warrior)))
            {
                Images.Add(typeof(Warrior), new Image("Warrior.jpg"));
            }
            return Images[typeof(Warrior)];
        }

        public static Image CreateWizzard()
        {
            if (!Images.ContainsKey(typeof(Wizzard)))
            {
                Images.Add(typeof(Wizzard), new Image("Wizzard.jpg"));
            }
            return Images[typeof(Wizzard)];
        }

    }
}
