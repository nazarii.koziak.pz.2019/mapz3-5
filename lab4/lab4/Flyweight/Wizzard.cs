﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab4.Flyweight
{
    class Wizzard : Unit
    {
        public Wizzard()
        {
            Name = "CoolWizrd228";
            Health = 10;
            Damage = 40;
            picture = UnitFlyweight.CreateWizzard();
        }

    }
}
