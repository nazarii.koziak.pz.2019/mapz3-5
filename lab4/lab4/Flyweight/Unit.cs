﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace lab4.Flyweight
{
    abstract class Unit
    {
            public string Name { get; protected set; }
            public int Health { get; protected set; }
            public int Damage { get; protected set; } 
            public Image picture { get; set; }
        public virtual void Voice()
        {
            Console.WriteLine($"I`m a {Name} and I will destroy YOU!");
        }
    }

    class Image
    {
        private string path;
        public void Load(string _path)
        {
            path = _path; 
            Console.WriteLine("Picture is loaded");
        }

        public Image(string path)
        {
            this.path = path;
        }
    }

    
}
