﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using lab5.Strategy;

namespace lab5.Observer
{
    class SubObs : ISubject
    {

        public List<IObserver> Observers = new List<IObserver>(); 

        public void AttachObserver(IObserver observer)
        {
            Observers.Add(observer);
        }

        public void DetachObserver(IObserver observer)
        {
            Observers.Remove(observer);
        }

        public void Notify() // сповіщення про подію
        {
            Console.WriteLine("Subject: Notifiying observers...");
            foreach (var ob in Observers)
                ob.Update(this);
        }

    }
}
