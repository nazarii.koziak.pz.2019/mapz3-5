﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using lab5.Strategy;
using lab5.Observer;
using lab5.Mediator;

namespace lab5
{
    class Program
    {
        static void Main(string[] args)
        {
            ConcreteMediator mediator = ConcreteMediator.GetInstance();
            User guest1 = new User(mediator);
            User guest2 = new User(mediator);
            mediator.AddUser(guest1);
            mediator.AddUser(guest2);


            Console.WriteLine("Choose profile:\n1 - Guest\n2 - Admin");

            Console.Write("Enter your choose: ");
            int choose = Convert.ToInt32(Console.ReadLine());

            switch (choose)
            {
                case 1:
                    break;
                case 2:
                    guest1.ChangeStrategy(new Admin());
                    break;
                default:
                    Console.WriteLine("Incorrect data!");
                    break;
            }
            guest1.UserMenu();
            guest1.UserMenu();

            
        }
    }
}
