﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using lab5.Observer;
using lab5.Strategy;

namespace lab5.Mediator
{
    class ConcreteMediator : IMediator
    {
        private ConcreteMediator() { }
        private static ConcreteMediator instance;
        public static ConcreteMediator GetInstance()
        {
            if(instance == null)
            {
                instance = new ConcreteMediator();
            }
            return instance;
        }

        public SubObs obs = new SubObs();      

        private List<IObserver> users = new List<IObserver>();

       public void AddUser(User user)
       {
            users.Add(user);
            obs.AttachObserver(user);
       }

        public void DeleteUser(User user)
        {
            users.Remove(user);
            obs.DetachObserver(user);
        }

        public void Notify(object sender, int score)
        {
            if(score > Info.BestScore)
            {
                Info.BestScore = score;
                Console.WriteLine("New best score! We will inform everyone!");
            }
            obs.Notify();
        }


    }
}
