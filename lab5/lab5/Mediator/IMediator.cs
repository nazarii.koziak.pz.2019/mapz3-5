﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab5.Mediator
{
    interface IMediator
    {
        void Notify(object sender, int score);
    }
}
