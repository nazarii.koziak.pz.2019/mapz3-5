﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab5.Strategy
{ 

    class Guest : IUserStrategy
    {
        public void GetInfoAboutApp()
        {
            Console.WriteLine(Info.URLtoInfo);
        }

        public void GetInfoAboutContact()
        {
            Console.WriteLine(Info.ContactNumber);
        }

        public void GetInfoAboutPrivacy()
        {
            Console.WriteLine(Info.Privacy);
        }

        public void GetInfoAboutSocialMedia()
        {
            foreach (var s in Info.SocialMedia)
                Console.WriteLine(s);
        }
        public int PlayGame()
        {
            Random rand = new Random();
            return rand.Next(1, 10000);
        }

        public void PrintUserMenu()
        {
            Console.WriteLine("1 - Open info about app");
            Console.WriteLine("2 - Open contact info");
            Console.WriteLine("3 - Open privacy info");
            Console.WriteLine("4 - Open social media info");
            Console.WriteLine("5 - buy subscribe!");
            
        }

        public bool SelectUserMenu()
        {
            Console.Write("Enter your choose: ");
            int choose = Convert.ToInt32(Console.ReadLine());

            switch (choose)
            {
                case 1:
                    GetInfoAboutApp();
                    break;
                case 2:
                    GetInfoAboutContact();
                    break;
                case 3:
                    GetInfoAboutPrivacy();
                    break;
                case 4:
                    GetInfoAboutSocialMedia();
                    break;
                case 5:
                    return true;
                default:
                    Console.WriteLine("Incorrect data");
                    break; 
                       
            }
            return false;
        }
    }
}
