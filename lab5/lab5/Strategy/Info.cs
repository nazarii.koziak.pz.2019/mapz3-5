﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab5.Strategy
{
    static class Info
    {
        public static int BestScore = 0;
        public static string URLtoInfo = "https://HelloWorld.com";
        public static string ContactNumber = "88005553535";
        public static string Privacy = "Protected by UA goverment!";
        public static string[] SocialMedia =
        {
            "instagram: @HelloWorld",
            "facebook: hello world!",
            "twitter: Im Crying",
            "linkedIn: Cool App228"
        };
    }
}
