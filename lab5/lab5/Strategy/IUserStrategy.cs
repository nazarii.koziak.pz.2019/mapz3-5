﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab5.Strategy
{
    interface IUserStrategy
    {
        void PrintUserMenu();
        bool SelectUserMenu();
        void GetInfoAboutApp();
        void GetInfoAboutContact();
        void GetInfoAboutPrivacy();
        void GetInfoAboutSocialMedia();
        int PlayGame();
        
    }
}
