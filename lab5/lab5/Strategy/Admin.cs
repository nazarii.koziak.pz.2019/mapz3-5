﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab5.Strategy
{
    class Admin : IUserStrategy, ISubBonus
    {
        public void GetInfoAboutApp()
        {
            Console.WriteLine(Info.URLtoInfo);
        }

        public void GetInfoAboutContact()
        {
            Console.WriteLine(Info.ContactNumber);
        }

        public void GetInfoAboutPrivacy()
        {
            Console.WriteLine(Info.Privacy);
        }

        public void GetInfoAboutSocialMedia()
        {
            foreach (var s in Info.SocialMedia)
                Console.WriteLine(s);
        }

        public void SomeCoolBonus()
        {
            Console.WriteLine("You are too cool Admin!");
        }

        public void ChangeInfoAboutApp()
        {
            Console.Write("Enter url: ");
            string Url = Console.ReadLine() ;
            Info.URLtoInfo = Url;
            Console.WriteLine($"new Url is: {Url}");     
        }

        public void ChangeInfoAboutContact()
        {
            Console.Write("Enter new number: ");
            string number = Console.ReadLine();
            Info.ContactNumber = number;
            Console.WriteLine($"new contact number is: {number}");
        }

        public void ChangeInfoAboutPrivacy()
        {
            Console.Write("Enter new privacy: ");
            string info = Console.ReadLine();
            Info.Privacy = info;
            Console.WriteLine($"New privacy: {info}");
        }
        public int PlayGame()
        {
            Random rand = new Random();
            return rand.Next(1, 10000);
        }


        public void PrintUserMenu()
        {
            Console.WriteLine("0 - Play game");
            Console.WriteLine("1 - Open info about app");
            Console.WriteLine("2 - Open contact info");
            Console.WriteLine("3 - Open privacy info");
            Console.WriteLine("4 - Open social media info");
            Console.WriteLine("5 - Use cool bonus!");
            Console.WriteLine("6 - Change info about app");
            Console.WriteLine("7 - Change contact info");
            Console.WriteLine("8 - Change privacy info");
            
           
        }
        public bool SelectUserMenu()
        {
            Console.Write("Enter your choose: ");
            int choose = Convert.ToInt32(Console.ReadLine());

            switch (choose)
            {
                case 0:
                    return true;      
                case 1:
                    GetInfoAboutApp();
                    break;
                case 2:
                    GetInfoAboutContact();
                    break;
                case 3:
                    GetInfoAboutPrivacy();
                    break;
                case 4:
                    GetInfoAboutSocialMedia();
                    break;
                case 5:
                    SomeCoolBonus();
                    break;
                case 6:
                    ChangeInfoAboutApp();
                    break;
                case 7:
                    ChangeInfoAboutContact();
                    break;
                case 8:
                    ChangeInfoAboutPrivacy();
                    break;
                default:
                    Console.WriteLine("Incorrect data");
                    break;

            }
            return false;
        }
    }
}
