﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using lab5.Observer;
using lab5.Mediator;

namespace lab5.Strategy
{
    class User : IObserver
    {
        private int myScore;
        private IMediator mediator;
        private IUserStrategy strat = new Guest();
        private bool SubStatus { get; set; } = false; 
        public User(IMediator _mediator = null)
        {
            this.mediator = _mediator;
        }

        public void SetMediator(IMediator mediator)
        {
            this.mediator = mediator;
        }


        public void ChangeStrategy(IUserStrategy strategy)
        {
            strat = strategy;

            if(strategy is PremiumGuest || strategy is Admin)
            {
                SubStatus = true;
            }
        }
        public void Update(ISubject subject) // Реакція на подію
        {
            if(myScore == Info.BestScore)
            {
                Console.WriteLine("Winner, winner chicken dinner!\n\n");
            }
            else
            {
                Console.WriteLine("I`ll do my best!\n\n");
            }
        }

        public void UserMenu()
        {
            strat.PrintUserMenu();
            if (!SubStatus && strat.SelectUserMenu())
            {
                SubStatus = true;
                ChangeStrategy(new PremiumGuest());
                Console.WriteLine("\n\n Done! You have new bonus!");
            }
            else if(strat.SelectUserMenu() && SubStatus)
            {
                int result = strat.PlayGame();
                myScore = result;
                Console.WriteLine($"Your score is: {result}");
                mediator.Notify(this, result);
            }
        }

    }
}
