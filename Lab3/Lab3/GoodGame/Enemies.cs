﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3.GoodGame
{
    class Warrior : IEnemy
    {
        public void Attack()
        {
            Console.WriteLine("Hit you by sword");
        }

        public void Special()
        {
            Console.WriteLine("Berserker mode");
        }

        public void BatleRoar()
        {
            Console.WriteLine("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
        }

        public override string ToString()
        {
            return "Warrior";
        }
    }
    class Archer : IEnemy
    {
        public void Attack()
        {
            Console.WriteLine("Shooting you by bow");
        }

        public void Special()
        {
            Console.WriteLine("Sniper mode");
        }

        public void BatleRoar()
        {
            Console.WriteLine("Pain - Ugly, Ugly - Pain");
        }

        public override string ToString()
        {
            return "Archer";
        }
    }
    class Wizzard : IEnemy
    {
        public void Attack()
        {
            Console.WriteLine("SUNSTRIKE!");
        }

        public void Special()
        {
            Console.WriteLine("Avatar mode!");
        }

        public void BatleRoar()
        {
            Console.WriteLine("I have many names, but my true name of power is... Karl...");
        }
        public override string ToString()
        {
            return "Wizzard";
        }
    }
}
