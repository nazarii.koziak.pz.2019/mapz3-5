﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Lab3.GoodGame
{
    class BattleField
    {
        private IEnemy[] enemies;
        private Tree[] trees;
        private Bench[] benches;

        public BattleField(IEnemy[] enemies, Tree[] trees, Bench[] benches)
        {
            this.enemies = enemies;
            this.trees = trees;
            this.benches = benches;
        }

        public void EnemiesInfo()
        {
            var warrior = enemies.Where((item) =>
            {
                return item.ToString() == "Warrior";
            }).Count();

            Console.WriteLine($"Warriors: {warrior}");

            var arch = enemies.Where((item) =>
            {
                return item.ToString() == "Archer";
            }).Count();

            Console.WriteLine($"Archers: {arch}");

            var wizz = enemies.Where((item) =>
            {
                return item.ToString() == "Wizzard";
            }).Count();

            Console.WriteLine($"Wizzards: {wizz}");

        }
    }
}
