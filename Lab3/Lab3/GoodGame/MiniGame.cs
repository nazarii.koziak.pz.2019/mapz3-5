﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab3.GoodGame;

namespace Lab3
{
    class MiniGame //fabric
    {
        private BattleField location;
        private Hero player;

        public MiniGame(IFactory factory)
        {
            player = new Hero();

            Tree[] trees = new Tree[]
            {
                new Tree(),
                new Tree(),
                new Tree()
            };

            Bench[] benches = new Bench[]
            {
                new Bench(),
                new Bench(),
                new Bench()
            };

            IEnemy[] enemies = new IEnemy[100];

            for(int i = 0; i < enemies.Length; i++)
            {
                enemies[i] = factory.Create();
            }

            location = new BattleField(enemies, trees, benches);
        }

        public void StartGame()
        {
            Console.WriteLine("Loading location...");
            Console.WriteLine("Loading trees...");
            Console.WriteLine("Puting benches...");
            Console.WriteLine("Creating enemies...");
            Console.WriteLine("Player loaded...");
            Console.WriteLine("Ну вьсо, гра почалась, тiкай з СЕЛА!");

            location.EnemiesInfo();

        }
    }
   

}
