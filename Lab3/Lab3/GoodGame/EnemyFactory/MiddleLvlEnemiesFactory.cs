﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3.GoodGame.EnemyFactory
{
    class MiddleLvlEnemiesFactory : IFactory
    {
        private static int strongEn = 0;

        public IEnemy Create()
        {
            if(strongEn < 10)
            {
                strongEn += 1;
                return new Wizzard();
            }

            IFactory ezFact = new EzLvlEnemiesFactory();
            return ezFact.Create();
        }

        public override string ToString()
        {
            return "Середній рівень складності";
        }
    }
}
