﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3.GoodGame.EnemyFactory
{
    class EzLvlEnemiesFactory : IFactory
    {
        private static Random rand = new Random();

        public IEnemy Create()
        {
            string[] enemies = new string[]
            {
                "War",
                "Arch",
                "Wizz"
            }; 

            switch(enemies[rand.Next(0, enemies.Length)])
            {
                case "War":
                    return new Warrior();
                case "Arch":
                    return new Archer();
                case "Wizz":
                    return new Wizzard();
                default:
                    throw new Exception("Таких класів не існує D:");
            }
        }

        public override string ToString()
        {
            return "Легкий рівень складності";
        }
    }
}
