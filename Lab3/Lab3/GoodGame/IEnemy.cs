﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3.GoodGame
{
    interface IEnemy
    {
        void Attack();
        void Special();
        void BatleRoar();
    }
}
