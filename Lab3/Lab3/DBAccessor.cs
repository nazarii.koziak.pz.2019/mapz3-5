﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3
{
    class DBAccessor //singleton
    {
        private static DBAccessor instance;
        private DBAccessor() { }

        public static DBAccessor getInstance()
        {
            if (instance == null)
                instance = new DBAccessor();

            return instance;
        }

        public string sqlConnection { get; protected set; }

        public void setConnection() => Console.WriteLine("Conection successful created!");
        public void closeConnection() => Console.WriteLine("Closing connection...\n...\n...\n...\nOK!");

        public void SendData() => Console.WriteLine("Sending data...");

    }
}
