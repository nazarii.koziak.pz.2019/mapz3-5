﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab3.GoodGame.EnemyFactory;
using Lab3.AbstractFabric;

namespace Lab3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("1 - play game\n2 - open feedbacks\n3 - open settings");
            Console.Write("your choose: ");
            int choose = Convert.ToInt32(Console.ReadLine());

            switch(choose)
            {
                case 1:
                    Console.WriteLine("Starting game");
                    break;
                case 2:
                    Console.WriteLine("Open Feedbacks");
                    break;
                case 3:
                    Console.WriteLine("Open settings");
                    break;
            }
            
            if(choose == 1)
            {
                var mg = new MiniGame(new EzLvlEnemiesFactory());
                mg.StartGame();
                Console.WriteLine("\n\n\nYour score: 228 points. Send result to server");
                var DB = DBAccessor.getInstance();

                DB.SendData();
            }
            else
            {
                var user = new User();
                user.Start();
            }
            
        }
    }

}
